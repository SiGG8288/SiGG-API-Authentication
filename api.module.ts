import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { CreateDidAuthRequestService } from './api/createDidAuthRequest.service';
import { IssuerService } from './api/issuer.service';
import { MessagesService } from './api/messages.service';
import { RetrieveSharedCredentialService } from './api/retrieveSharedCredential.service';
import { RevocationService } from './api/revocation.service';
import { SignJWTService } from './api/signJWT.service';
import { UsersService } from './api/users.service';
import { VerifierService } from './api/verifier.service';
import { WalletService } from './api/wallet.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    CreateDidAuthRequestService,
    IssuerService,
    MessagesService,
    RetrieveSharedCredentialService,
    RevocationService,
    SignJWTService,
    UsersService,
    VerifierService,
    WalletService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<ApiModule> {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
