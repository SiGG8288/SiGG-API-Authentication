/**
 * SiGG API-Service
 * SiGG API-Service
 *
 * OpenAPI spec version: 1.0.0
 * Contact: SiGGAPI@siggconstruction.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface Manufacturer { 
    name: string;
    homePage?: string;
    phone?: string;
}